var bootState = {
    create: function() {
    // Set some game settings.
    game.stage.backgroundColor = '#404040';
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.renderer.renderSession.roundPixels = true;
    // Start the load state.
    game.state.start('load');
    }
}; 