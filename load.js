var loadState = {
    preload: function () {
        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Load all game assets
        game.load.crossOrigin = 'anonymous';
        game.load.spritesheet('player', 'assets/player.png', 32, 32);
        game.load.image('wall', 'assets/wall.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('normal', 'assets/normal.png');
        game.load.image('nails', 'assets/nails.png');
        game.load.image('btn', 'assets/btn.png');
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
        game.load.audio('dead', ['music/dead.mp3', 'music/dead.ogg']);
        game.load.audio('fake', ['music/fake.mp3', 'music/fake.ogg']);
        game.load.audio('hurt', ['music/hurt.mp3', 'music/hurt.ogg']);
        game.load.audio('normal', ['music/normal.mp3', 'music/normal.ogg']);
        game.load.audio('tramp', ['music/tramp.mp3', 'music/tramp.ogg']);
    },
    create: function() {
        game.stage.backgroundColor = '#404040';
        // Go to the menu state
        game.state.start('menu');
    }
}; 