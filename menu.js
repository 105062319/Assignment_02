var menuState = {
    create: function() {
        // Add a background image
        //game.add.image(0, 0, 'background');
        
        // Display the name of the game
        var nameLabel = game.add.text(game.width/2, 0, '小朋友下樓梯',{ font: '50px Arial', fill: '#ffffff' });
        nameLabel.anchor.setTo(0.5, 0.5);
        var tween = game.add.tween(nameLabel);
        tween.to({y:90}, 800);
        tween.easing(Phaser.Easing.Bounce.Out);
        tween.start();

        // Show the score at the center of the screen
        //var scoreLabel = game.add.text(game.width/2, game.height/2,'score: ' + game.global.score, { font: '25px Arial', fill: '#ffffff' });
        //scoreLabel.anchor.setTo(0.5, 0.5);
        var playerpic = game.add.sprite(game.width/2, 160,'player');
        var normalpic = game.add.sprite(game.width/2, 194, 'normal');
        var normalpic2 = game.add.sprite(game.width/2-90, 244, 'normal');
        var nailpic = game.add.sprite(240, 334, 'nails');
        // Explain how to start the game
        var startLabel = game.add.text(game.width/2-30, game.height-100,'press the up arrow key to start', { font: '25px Arial', fill: '#ffffff' });
        startLabel.anchor.setTo(0.5, 0.5);
        var tween2 = game.add.tween(startLabel);
        tween2.to({x:game.width/2}, 300);
        tween2.start();

        var btn = game.add.button(30,350,'btn',function(){alert("使用左右來操縱小朋友\n當觸碰到尖刺時會減少生命值\n若是踩踏到藍色樓梯或灰色樓梯會增加生命值\n彈簧樓梯有可能會跳高到處碰尖刺\n但也可利用彈簧樓梯重複踩踏藍色和灰色樓梯\n生命值的最大值為10\n當生命小於1或是樓梯踩空就會死亡\n遊戲愉快:)")});

        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        upKey.onDown.add(this.start, this);
    },
    start: function() {
        // Start the actual game
        game.state.start('play');
    },
}; 